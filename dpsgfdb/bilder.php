<?php
/*
Template Name: Bilderseiten-Template

Diese Seite wird verwendet, um das alte Bilderarchiv anzuzeigen.

Die Albumstruktur und Bildernamen sind alle im DocumentRoot/fotoarchiv_alte_seite/fotoarchiv.json File abgelegt.
Diese Seite parsed das Json-File und baut die Liste zusammen.
Die Bilder und Thumbnails werden erst geladen, wenn man das jeweilige Album auswählt.
*/
?>

<?php get_header(); ?>
<div id="pagemain">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="completepost">
			<div class="posthead">	
				<div class="starticon"></div><!-- starticon-->
   				<h2 class="postheading"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

				<div class="floatstop"></div>
			</div><!--posthead-->
	   		<div class="entry">
      				<!--<?php the_content(); ?>-->
				<p>Bitte auf das jeweilige Album klicken, um Bilder anzuzeigen.</p>
				<!--Bilderapp-->
				<div ng-app="bilderseite" ng-controller="bilderseiteCtrl">
					<ul class="albumList">
    						<li ng-repeat="album in fotoarchiv" ng-click="album.active = !album.active; albumImages = album.images">
        					<span>{{album.albumTitle}}&nbsp;&nbsp;&nbsp;<b style="float:right;">{{album.dateMonth | replaceMonthMaerz}} {{album.dateYear}}</b></span>
        					<ul class="thumbList">
            						<li ng-repeat="foto in albumImages" ng-show="album.active">
								<a href="/fotoarchiv_alte_seite/fotoalbum/{{album.dirName}}/{{foto}}"><img src="/fotoarchiv_alte_seite/fotoalbumThumbs/{{album.dirName}}/{{foto | replaceFileExtByJpg}}"></a>
            						</li>
        					</ul>
						<div class="floatstop"></div>
    					</li>
						</ul>
  						<script src="/angular/angular.js"></script>
					<script>
						'use strict';

						/* Controller */
						var bilderseite = angular.module('bilderseite', []);

						bilderseite.controller('bilderseiteCtrl', ['$scope', '$http', function($scope, $http) {
						$http.get('/fotoarchiv_alte_seite/fotoarchiv.json').success(function(data) {
							$scope.fotoarchiv = data;
						});

  						$scope.setMaster = function(obj, $event){
    							console.log($event.target);
  						}

						$scope.showAlbum = function() {
							$scope.active = !$scope.active
						};
						}]);

						/* Filters */
						bilderseite.filter('replaceFileExtByJpg', function () {
    						return function (item) {
      							return item.replace(/\.(JPG|jpeg|gif)/g, '.jpg');
    						}
  						});

						bilderseite.filter('replaceMonthMaerz', function () {
    						return function (item) {
      							return item.replace(/(M&auml;rz)/g, 'März');
    						}
  						});

		
					</script>

				</div>
				<!--/Bilderapp-->
      			</div><!-- entry -->
			<div class="floatstop"></div>

				<div class="zielicon"></div><!--zielicon-->

			<div class="postseperator"></div>
		</div><!--completepost-->
      	<?php endwhile; ?>
  
      <?php endif; ?>

	</div><!-- pagemain -->
<div class="floatstop"></div>
<?php get_footer(); ?>
